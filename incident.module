<?php

/**
 * @file
 * Allows users to create and submit incident reports.
 */

/**
 * Implementation of hook_node_info().
 */
function incident_node_info() {
  return array(
    'incident' => array(
      'name' => t('Incident report'),
      'module' => 'incident',
      'description' => t('An incident report serves as an official record of notice of the occurrence of an incident and the details thereof. Filing a fraudulent incident report is a criminal offense.'),
      'has_title' => TRUE,
      'title_label' => t('Location of incident'),
      'has_body' => TRUE,
      'body_label' => t('Incident details'),
      'min_word_count' => 1,
      'locked' => TRUE,
    ),
  );
}

/**
 * Implementation of hook_perm().
 */
function incident_perm() {
  return array('create Incident report', 'edit own Incident report', 'edit any Incident report', 'delete own Incident report', 'delete any Incident report');
}

/**
 * Implementation of hook_access().
 */
function incident_access($op, $node, $account) {
  $is_author = $account->uid == $node->uid;
  switch ($op) {
    case 'create':
      return user_access('create Incident report', $account);

    case 'update':
      return user_access('edit own Incident report', $account) && $is_author ||
        user_access('edit any Incident report', $account);

    case 'delete':
      return user_access('delete own Incident report', $account) && $is_author ||
        user_access('delete any Incident report', $account);
  }
}

/**
 * Implementation of hook_form().
 */
function incident_form($node) {
  $type = node_get_types('type', $node);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => check_plain($type->title_label),
    '#required' => TRUE,
    '#default_value' => $node->title,
    '#weight' => -5,
    '#maxlength' => 255,
  );
  $form['body_filter']['body'] = array(
    '#type' => 'textarea',
    '#title' => check_plain($type->body_label),
    '#default_value' => $node->body,
    '#rows' => 7,
    '#required' => TRUE,
  );
  $form['body_filter']['filter'] = filter_form($node->format);
  $form['party'] = array(
    '#type' => 'textfield',
    '#title' => t('Reporting party name and agency'),
    '#required' => TRUE,
    '#default_value' => isset($node->party) ? $node->party : '',
    '#weight' => 5
  );
  return $form;
}

/**
 * Implementation of hook_validate().
 */
function incident_validate($node, &$form) {
  if (isset($node->party) && str_word_count($node->party) < 2) {
    $type = node_get_types('type', $node);
    form_set_error('party', t('You must provide your name and that of your agency. If you do not wish to disclose this information you must enter "Anonymous Anonymous" instead.'));
  }
}

/**
 * Implementation of hook_insert().
 */
function incident_insert($node) {
  db_query("INSERT INTO {incident} (nid, vid, party) VALUES (%d, %d, '%s')",
    $node->nid, $node->vid, $node->party);
}

/**
 * Implementation of hook_update().
 */
function incident_update($node) {
  if ($node->revision) {
    incident_insert($node);
  }
  else {
    db_query("UPDATE {incident} SET party = '%s' WHERE vid = %d",
      $node->party, $node->vid);
  }
}

/**
 * Implementation of hook_delete().
 */
function incident_delete(&$node) {
  db_query('DELETE FROM {incident} WHERE nid = %d', $node->nid);
}

/**
 * Implementation of hook_load().
 */
function incident_load($node) {
  return db_fetch_object(db_query('SELECT party FROM {incident} WHERE vid = %d',
    $node->vid));
}

/**
 * Implementation of hook_view().
 */
function incident_view($node, $teaser = FALSE, $page = FALSE) {
  if (!$teaser) {
    $node = node_prepare($node, $teaser);

    $node->content['party'] = array(
      '#value' => theme('incident_party', $node),
      '#weight' => 2
      );
  }
  if ($teaser) {
    $node = node_prepare($node, $teaser);
}

  return $node;
}

/**
 * Implementation of hook_theme().
 */
function incident_theme() {
  return array(
    'incident_party' => array(
      'arguments' => array('node'),
    ),
  );
}

function theme_incident_party($node) {
  $output = '<div class="incident-party">'. check_markup($node->party) .'</div>';
  return $output;
}
